﻿using DBAccess;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace FamilliarsRecognition
{
    class TUI : IInterface
    {
        Presenter presenter;
        List<string> peopleToDisplay;

        public TUI()
        {
            presenter = new Presenter(this);
            peopleToDisplay = new List<string>();
        }

        public void Run()
        {
            while (!presenter.Open(EnterPath()))
                Console.WriteLine("Can not open entered directory");
            string input;
            do
            {
                MainMenu();
                input = Console.ReadLine();
                switch (input)
                {
                    case "0":
                        break;
                    case "1":
                        presenter.NextImage();
                        break;
                    case "2":
                        presenter.PrevImage();
                        break;
                    case "3":
                        ShowPeople();
                        break;
                    case "4":
                        SelectPerson();
                        break;
                    case "5":
                        if (!presenter.Open(EnterPath()))
                            Console.WriteLine("Can not open entered directory");
                        break;
                    default:
                        Console.WriteLine("Please, enter a number of menu option (example: enter '0' to exit).");
                        break;
                }
            } while (input != "0");
            presenter.Dispose();
        }

        private void SelectPerson()
        {
            string input;
            int index = 0;
            bool wrongInput = true;
            do
            {
                Console.WriteLine("Enter person number in list or 0 to cancel:");
                input = Console.ReadLine();
                try
                {
                    index = (int)uint.Parse(input);
                    wrongInput = false;
                }
                catch (FormatException)
                {
                    Console.Write("Wrong input. ");
                    EventLog.Log("User couldn't type a number", EventLog.Type.Exception);
                }
            } while (wrongInput);
            if (index != 0)
                presenter.OpenPerson(index - 1);
        }

        private void ShowPeople()
        {
            int i = 0;
            foreach (string person in peopleToDisplay)
                Console.WriteLine(++i + ". " + person);
        }

        private void MainMenu()
        {
            Console.WriteLine("Enter a number of one of the following options:");
            Console.WriteLine("0. Exit");
            Console.WriteLine("1. Next image");
            Console.WriteLine("2. Previous image");
            Console.WriteLine("3. View people list");
            Console.WriteLine("4. Select a person");
            Console.WriteLine("5. Change directory");
        }

        private string EnterPath()
        {
            Console.WriteLine("Enter path to images directory:");
            return Console.ReadLine();
        }

        public void AddPerson(Person person)
        {
            peopleToDisplay.Add(person.ToString());
        }

        public void ChangeImage(in Image img)
        {
            if (img.Tag != null)
                Console.WriteLine("Image changed: " + img.Tag.ToString());
        }

        public void ChangePerson(int index, Person person)
        {
            peopleToDisplay[index] = person.ToString();
        }

        public void ClearPeople()
        {
            peopleToDisplay.Clear();
        }

        public void OpenPersonEditor(Person person)
        {
            TextPersonEditor editor = new TextPersonEditor(person, presenter);
            editor.EditPerson();
        }        
    }
}
